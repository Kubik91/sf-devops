FROM postgres

ENV POSTGRES_HOST=postgres
ENV POSTGRES_PORT=5432
ENV POSTGRES_USER=test
ENV POSTGRES_PASSWORD=password
ENV POSTGRES_DB=test
ENV PG_MAJOR=14
ENV TZ=UTC

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir /usr/local/pgsql && \
    mkdir /var/lib/pgsql && \
    chown postgres -R /usr/local/pgsql && \
    chown postgres -R /var/lib/pgsql && \
    chown postgres -R /var/lib/postgresql && \
    chown postgres -R /var/run/postgresql

USER postgres
RUN echo "local all all trust" >> /var/lib/pgsql/pg_hba.conf && \
    echo "host all all 172.17.0.1\16 md5" >> /var/lib/pgsql/pg_hba.conf && \
    initdb -D /usr/local/pgsql/data && \
    pg_createcluster $PG_MAJOR main --start && \
    psql --command "CREATE USER $POSTGRES_USER WITH SUPERUSER PASSWORD '$POSTGRES_PASSWORD';" && \
    psql --command "CREATE DATABASE $POSTGRES_DB;" && \
    psql --command "GRANT ALL PRIVILEGES ON DATABASE $POSTGRES_DB TO $POSTGRES_USER;"

EXPOSE $POSTGRES_PORT

CMD ["postgres"]